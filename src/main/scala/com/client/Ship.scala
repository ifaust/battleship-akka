package com.client

import akka.actor.{Actor, ActorRef}
import com.messages.Hit

class Ship(val length: Int) extends Actor {

  def build(): Unit = ???

  val occupiedCells: List[ActorRef] = ???

  def receive = {
    case Hit => {
      print("Hit")
    }
  }

}
