package com.client

import scala.concurrent.ExecutionContextExecutor
import akka.actor._
import com.messages.Player
import com.messages.{ConnectClient, FireOnCell, YourTurn}
import com.client.PlayerActor.Start
import com.client.Grid.PlaceShipOnGrid
import akka.pattern._
import akka.util.Timeout
import scala.concurrent.duration._

object PlayerActor {

  case object Start

}


class PlayerActor(server: ActorRef) extends Actor {
  implicit val timeout = Timeout(20 seconds)

  implicit def dispatcher: ExecutionContextExecutor = context.dispatcher

  val player = Player(context.props.toString)
  val grid = context.actorOf(Props(classOf[Grid], 10, 10))

  def receive = {
    case place: PlaceShipOnGrid =>
      grid ! place
    case Start =>

      server ! ConnectClient(player)
      context.become(gridReady(grid))
  }

  def gridReady(grid: ActorRef): Receive = {

    case YourTurn =>
    case fire: FireOnCell => (grid ? fire) pipeTo sender()
  }
}