package com

import akka.actor.{ActorSystem, Props}
import com.messages.BeginGame
import com.server.BattleshipServer
import com.typesafe.config.ConfigFactory

object Battleship extends App {
  // start a server to for grids etc
  // get the grids to create cells
  // allow the user to place ships
  // start game

  println("Starting Battleship game server")
  val system = ActorSystem("Battleship", ConfigFactory.load.getConfig("battleshipserver"))
  val server = system.actorOf(Props[BattleshipServer], name = "battleshipserver")
  server ! BeginGame
}
