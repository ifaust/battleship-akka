name := "battleship"

version := "0.1"

scalaVersion := "2.12.2"

lazy val akkaV = "2.5.2"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.2",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.2" % Test,
  "com.typesafe.akka" %% "akka-remote" % "2.5.2",
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "com.googlecode.lanterna" % "lanterna" % "3.0.0-rc1"
)
